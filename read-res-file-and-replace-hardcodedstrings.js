var fs = require('fs');
var path = require('path');

readTabDelimitedFile = (filename) => {
  var res = fs.readFileSync(path.join(__dirname, filename), 'utf8');
  return res.split('\n').map(row => row.split('\t'))
}

console.log('Parsing tab delimited file...');


var data = readTabDelimitedFile('results newlines.txt');

replaceData = (data) => {
for (let i = 0; i < data.length; i++) {
  const el = data[i];
  const key = `@Terms.${el[0]}`;
  let val = el[1];
  val = val.replace('.','\\.');
  val = val.replace('?', '\\?');

  const filepath = el[2].trim();
  // console.log(filepath);
  const regExp = `>\\s*?${val}\\s*?<`;
  const regex = new RegExp(regExp, 'mgi');
  if(val.indexOf('?') > 0)
    console.log(regex.source);
  let content = fs.readFileSync(filepath, "utf-8");
  const result = content.replace(regex, `>${key}<`);
  fs.writeFileSync(filepath, result, 'utf8');
}
};

replaceData(data);