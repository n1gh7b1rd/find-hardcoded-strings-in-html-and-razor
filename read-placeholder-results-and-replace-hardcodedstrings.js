var fs = require('fs');
var path = require('path');

readTabDelimitedFile = (filename) => {
  var res = fs.readFileSync(path.join(__dirname, filename), 'utf8');
  return res.split('\n').map(row => row.split('\t'))
}

console.log('Parsing tab delimited file...');


var data = readTabDelimitedFile('place-holder-results.txt');

replaceData = (data) => {
for (let i = 0; i < data.length; i++) {
  const el = data[i];
  const key = `@Terms.${el[0]}`;
  const val = el[1];
  const filepath = el[2].trim();
  ///placeholder\s*=\s*"[A-Za-z@ë',!?.;\- ]*"/g;
  const regExp = `placeholder\\s*=\\s*\"${val}\"`;
  const regex = new RegExp(regExp, 'mgi');
  console.log(regex.source);
  let content = fs.readFileSync(filepath, "utf-8");
  const result = content.replace(regex, `placeholder = ${key}`);
  fs.writeFileSync(filepath, result, 'utf8');
}
};

replaceData(data);