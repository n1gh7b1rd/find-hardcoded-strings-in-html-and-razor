var fs = require('fs');
var regex = /placeholder\s*=\s*"[A-Za-z@ë',!?.;\- ]*"/g;
var results = [];
var walkSync = function(dir, filelist) {
  var path = path || require('path');
  var fs = fs || require('fs'),
      files = fs.readdirSync(dir);
  filelist = filelist || [];
  files.forEach(function(file) {
    if (fs.statSync(path.join(dir, file)).isDirectory()) {
      filelist = walkSync(path.join(dir, file), filelist);
    }
    else {
      filelist.push({filename: file, path: path.join(dir, file)});
    }
  });
  return filelist;
};

var filelist = walkSync("C:/Dev/Repositories/Umbrella/Webportal/Webportal/Webportal.Internet/Views");

// var fileNamesOccurrence= {};
// filelist.forEach(function(v, index) {
//   fileNamesOccurrence[v.filename] = !fileNamesOccurrence[v.filename] ? 1 : fileNamesOccurrence[v.filename] + 1;
// });
// var filesWithMoreThanOneOccurrenceWithHardcodedWords = {};
// for(var propertyName in data) {
//   if(parseInt(data[propertyName]) > 1){
//     console.log(propertyName + ': ' + data[propertyName]);
//   }
// }

console.log('Found ' + filelist.length + ' files. Parsing...');

for (let index = 0; index < filelist.length; index++) {
  const f = filelist[index];
  var content = fs.readFileSync(f.path, "utf-8");
  console.log('Parsing file ' + f.filename + ' at index ' + index);
  var newResults = content.match(regex);
  if(newResults && newResults.length>0){
    results = results.concat(newResults.map(r=>r.replace(/placeholder\s*=/gi, '').replace(/"/gi,'') + '\t' + f.path));
    // filesWithMoreThanOneOccurrenceWithHardcodedWords[f.filename] = fileNamesOccurrence[f.filename];
    console.log('Current results: ' + results.length);
  }
}

console.log(results);
console.log('Found ' + results.length + ' total results in ' + filelist.length + ' files. Writing to disk.');

var file = fs.createWriteStream('place-holder-results.txt');
file.on('error', function(err) { console.log(err) });

results.forEach(function(v, index) {
  file.write(v + '\n');
  console.log('Wrote result number ' + index + ' to file.');
});

console.log('Closing file.');
file.end();

// for(var propertyName in filesWithMoreThanOneOccurrenceWithHardcodedWords) {
//   if(parseInt(filesWithMoreThanOneOccurrenceWithHardcodedWords[propertyName]) > 1){
//     console.log(propertyName + ': ' + filesWithMoreThanOneOccurrenceWithHardcodedWords[propertyName]);
//   }
// }